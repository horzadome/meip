/*global jQuery,remotehost,chrome*/
"use strict";

jQuery.ajax({
    url: "https://ip.horza.org/api/ip",
    cache: true
})
    .done(function (data) {
        jQuery("#myip").append(data);
    });
