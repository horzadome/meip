/*global jQuery,remotehost,chrome*/
"use strict";

chrome.tabs.getSelected(null, function (tab) {
    var tablink = tab.url;
    function get_hostname(url) {
        var m = ((url || '') + '').match(/^https?:\/\/[^/]+/);
        return m ? m[0] : null;
    }
    // Used only when testing locally
    // var blahhost = "http://www.google.com/blah";

    var blahhost = get_hostname(tablink);
    if (blahhost === null) {
        blahhost = 'http://localhost/localhost';
    }
    var urlparts = blahhost.split('/');
    var remotehost = urlparts[2];

    jQuery("#hostname").text(remotehost);
    var se2 = document.createElement('script');
    se2.src = 'https://ip.horza.org/api/dig/' + remotehost;
    //jQuery inserts timestamp to appended children, thus busting my server cache :-/
    //jQuery('body').append(se2);
    document.body.appendChild(se2);

});
