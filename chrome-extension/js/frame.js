/*global jQuery,remotehost,chrome*/
"use strict";
jQuery(function () {
    jQuery("#tabs").tabs({
        beforeLoad: function (event, ui) {
            ui.jqXHR.error(function () {
                ui.panel.html(
                    "Couldn't load this tab."
                );
            });
        }
    });
});

function cliaddr(json) {
    jQuery("#myip").text(json);
}
function goog(json) {
    jQuery("#googip").text(json);
}
function odns(json) {
    jQuery("#odnsip").text(json);
}
function he(json) {
    jQuery("#heip").text(json);
}
function onic(json) {
    jQuery("#onicip").text(json);
}
function cf(json) {
    jQuery("#cfip").text(json);
}
