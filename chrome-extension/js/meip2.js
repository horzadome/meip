/*global jQuery,remotehost,chrome*/
"use strict";

function cliaddr(json) {
    jQuery("#myip").text(json);
}
function goog(json) {
    jQuery("#googip").text(json);
}
function odns(json) {
    jQuery("#odnsip").text(json);
}
function he(json) {
    jQuery("#heip").text(json);
}
function onic(json) {
    jQuery("#onicip").text(json);
}
function cf(json) {
    jQuery("#cfip").text(json);
}

chrome.tabs.getSelected(null, function (tab) {
    var tablink = tab.url;
    function get_hostname(url) {
        var m = ((url || '') + '').match(/^https?:\/\/[^/]+/);
        return m ? m[0] : null;
    }
    // var blahhost = "http://www.google.com/blah";
    var blahhost = get_hostname(tablink);
    var urlparts = blahhost.split('/');
    var remotehost = urlparts[2];
    var se2 = document.createElement('script');
    se2.src = 'https://ip.horza.org/api/dig/' + remotehost;
    jQuery('head').append(se2);
    jQuery("#hostname").text(remotehost);
});
