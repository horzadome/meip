from sanic import Sanic
from sanic.response import stream, json, text
import dns.resolver
# remove this in production
# from aoiklivereload import LiveReloader
# reloader = LiveReloader()
# reloader.start_watcher_thread()


app = Sanic()

@app.route("/api/ip", methods=["GET",])
async def get_clientip(request):
    # return text("IP is: %s and ua is %s" % (request.ip , request.headers.get("user-agent")))
    return text(request.headers.get("CF-Connecting-IP"))

@app.route("/api/dig/<dighost>", methods=["GET",])
async def dighost_handler(request, dighost):
    dig_target = '{}'.format(dighost)

    results = {}
    results['cliaddr'] = str(request.headers.get("CF-Connecting-IP"))

    # Google - Germany
    goog = dns.resolver.Resolver()
    goog.nameservers = ['8.8.8.8','8.8.4.4']
    goog.res = goog.query(dig_target, 'A')
    for ip in goog.res:
        results['goog'] = str(ip)

    # OpenNIC - US Texas
    onic = dns.resolver.Resolver()
    onic.nameservers = ['69.164.196.21','50.116.17.96']
    onic.res = onic.query(dig_target, 'A')
    for ip in onic.res:
        results['onic'] = str(ip)

    # HurricaneElectric - Germany
    he = dns.resolver.Resolver()
    he.nameservers = ['74.82.42.42']
    he.res = he.query(dig_target, 'A')
    for ip in he.res:
        results['he'] = str(ip)

    # OpenDNS Frankfurt
    odns = dns.resolver.Resolver()
    odns.nameservers = ['208.67.222.222','208.67.220.220']
    odns.res = odns.query(dig_target, 'A')
    for ip in odns.res:
        results['odns'] = str(ip)

    # Cloudflare - Frankfurt
    cf = dns.resolver.Resolver()
    cf.nameservers = ['1.1.1.1','1.0.0.1']
    cf.res = cf.query(dig_target, 'A')
    for ip in cf.res:
        results['cf'] = str(ip)

    result = ''.join("{!s}({!r});".format(k,v) for (k,v) in results.items())
    return text(
        result,
        headers={'Content-Type': 'application/javascript'},
        status=200)

@app.route("/api")
async def api(request):
    return text("hello API")

@app.route("/")
async def index(request):
    return text("hello world, go to /api")

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000, debug=False)
